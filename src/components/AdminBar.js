import React, { Component } from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';
import { NavLink as RRNavLink } from 'react-router-dom';

class AdminBar extends Component {
  render() {
    return (
      <div>
        <h1 className="text-center">Admin Panel</h1>
        <Nav className="align-items-center justify-content-center">
          <NavItem>
            <NavLink to="/admin/products" className="border rounded text-dark mx-1" tag={RRNavLink}>Product</NavLink>
          </NavItem>
          <NavItem>
            <NavLink to="/admin/categories" className="border rounded text-dark mx-1" tag={RRNavLink}>Category</NavLink>
          </NavItem>
          <NavItem>
            <NavLink to="/admin/orders" className="border rounded text-dark mx-1" tag={RRNavLink}>Order</NavLink>
          </NavItem>
          <NavItem>
            <NavLink to="/admin/customers" className="border rounded text-dark mx-1" tag={RRNavLink}>Customer</NavLink>
          </NavItem>
        </Nav>
      </div>
    );
  }
}

export default AdminBar;