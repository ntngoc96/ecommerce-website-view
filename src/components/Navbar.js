import React, { Component } from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';
import { NavLink as RRNavLink } from 'react-router-dom';

class Navbar extends Component {
  render() {
    return (
      <div>
        <Nav>
          <NavItem>
            <NavLink to="/" tag={RRNavLink}>Homepage</NavLink>
          </NavItem>
          <NavItem>
            <NavLink to="/products" tag={RRNavLink}>Product</NavLink>
          </NavItem>
          <NavItem>
            <NavLink to="/carts" tag={RRNavLink}>Cart</NavLink>
          </NavItem>
        </Nav>
      </div>
    );
  }
}

export { Navbar };