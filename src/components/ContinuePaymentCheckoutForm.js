import React, { Component } from 'react';
import Axios from 'axios';
import classNames from 'classnames';
import { CardElement, injectStripe } from 'react-stripe-elements';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

class ContinuePaymentCheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      complete: false,
      customer: {},
      name: '',
      email: '',
      amount: 0,
      creditCard: '',
      description: ''
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  async onSubmit(ev) {
    try {
      ev.preventDefault();
      const { name, email, amount, description } = this.state;
      let { token: stripeToken } = await this.props.stripe.createToken({ name: "Name" });
      const customerId = localStorage.getItem('customerId');
      const addressId = localStorage.getItem('addressId');
      const orderId = localStorage.getItem('orderId');
      const response = await Axios.post(`/api/v1/carts/checkout/oldOrder`, {
        name,
        email,
        amount,
        description,
        customerId,
        addressId,
        orderId,
        stripeToken: stripeToken.id
      });
      if (response.status === 200) {
        this.setState((state, props) => { return { complete: true } });

      }
    } catch (error) {
      console.log(error);

      alert("Checkout error")
    }
  }

  async componentDidMount() {
    try {
      let customerId = localStorage.getItem('customerId');
      let orderId = localStorage.getItem('orderId');
      let response = await Axios.get(`/api/v1/customers/${customerId}/orders/${orderId}`);
      let amount = 0;
      if (response.status === 200) {
        let { orderDetails } = response.data;
        for (let index = 0; index < orderDetails.length; index++) {
          amount += (orderDetails[index].unitPrice - (orderDetails[index].unitPrice * orderDetails[index].discount) / 100)* orderDetails[index].quantity
          amount = Math.round(amount * 1000) / 1000;
        }
        this.setState((state, props) => { return { amount } });

      } else {
        const error = new Error(response.error);
        throw error;
      }
    } catch (error) {
      console.log(error);
      alert("Loading cart amount error");
    }

  }


  handleInputChange = (event) => {
    const { value, name } = event.target;

    this.setState({
      [name]: value
    });
  }


  render() {
    let { name, email, amount, description } = this.state;
    if (this.state.complete) return <h1>Purchase Complete</h1>;
    return (
      <div className="checkout">
        <div className={classNames('text-center')}>
          <h1>Payment infomation</h1>
          <Form onSubmit={this.onSubmit}>
            <FormGroup>
              <Label for="name">Card's Holder</Label>
              <Input type="text" name="name" id="name" placeholder="eg. John Doe, Marcus Harley"
                value={name}
                onChange={this.handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input type="text" name="email" id="email" placeholder="eg. johndoe@gmail.com,asleyyoung@gmail.com"
                value={email}
                onChange={this.handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="amount">Amount</Label>
              <Input type="number" name="amount" id="amount" placeholder="eg. 100,200,222" disabled
                value={amount}
                onChange={this.handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Label for="description">Description</Label>
              <Input type="text" name="description" id="description" placeholder="Description about transaction"
                value={description}
                onChange={this.handleInputChange}
              />
            </FormGroup>
            <div id="card-element" className="bg-transparent text-white p-2 h-10 mb-4"></div>
            <CardElement />
            <Button onClick={this.onSubmit}>Purchase</Button>
          </Form>
        </div>
      </div>
    );
  }
}

export default injectStripe(ContinuePaymentCheckoutForm);
