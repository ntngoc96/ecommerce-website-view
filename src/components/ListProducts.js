import React, { Component } from 'react';
import {
  Row,
  Spinner
} from 'reactstrap';
import Axios from 'axios';
import Product from './Product';

class ListProducts extends Component {
  onItemClicked = async (value) => {
    try {
      await Axios.get(`/api/v1/carts/${value.productId}`);
    } catch (error) {
      alert('Add to cart error');
    }
  }
  
  render() {
    
    let { Items } = this.props ;

    return (
      <div>
        {Items.length === 0 && <Spinner color="primary" />}
        {Items.length !== 0 && <Row>{Items.map((value, index) =>
          <Product key={value.productId} {...value} type="view-mode" onAddToCartClicked={this.onItemClicked} />
        )}
        </Row>}
      </div>
    );
  }
}

export default ListProducts;
