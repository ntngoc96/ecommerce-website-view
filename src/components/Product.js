import React, { Component } from 'react';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button, Col
} from 'reactstrap';
import { NavLink } from "react-router-dom";
import classNames from 'classnames';
class Product extends Component {

  generateStar = (star = 3) => {
    let row = [];
    let index;
    for (index = 0; index < star; index++) {
      row.push(<span key={index} className="fa fa-star checked"></span>)
    }
    for (; index < 5; index++) {
      row.push(<span key={index} className="fa fa-star"></span>)
    }
    return row;
  }
  render() {
    let { name, producer, star, discount, unitPrice, productId, productImages, onDeleteClicked, type, onAddToCartClicked } = this.props;

    return (
      <Col xs="3" className={classNames('my-2')}>
        <Card>
          <CardImg className={classNames('w-20r', 'h-20r')} src={productImages[0].imageUrl} alt="Card image cap" />
          <CardBody>
            <CardTitle>{name}</CardTitle>
            <CardSubtitle>{producer}</CardSubtitle>
            {star === undefined && <CardText>{this.generateStar()} </CardText>}
            {star !== undefined && <CardText>{this.generateStar(star)}</CardText>}
            <CardText className={classNames('font-weight-light', 'u-text-line-though', { 'u-opacity-0': discount === 0 })}>US $ {unitPrice}</CardText>
            <CardText className={classNames('font-weight-bold')}>US $ {unitPrice - unitPrice * discount / 100}</CardText>
            {type === 'edit-mode' && <div className={classNames('text-center')}>
              <Button className={classNames('btn', 'btn-danger', 'w-40', 'mx-2')} onClick={e => onDeleteClicked(productId)}>Delete</Button>
              <NavLink className={classNames('btn', 'btn-primary', 'w-40', 'mx-2')} to={`/admin/products/${productId}`} >Edit</NavLink>
            </div>}
            {type === 'view-mode' && <Button onClick={e => onAddToCartClicked(this.props)}>Add to cart</Button>}
          </CardBody>
        </Card>
      </Col>
    );
  }
}

export default Product;