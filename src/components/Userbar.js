import React, { Component } from 'react';
import { Nav, NavItem, NavLink } from 'reactstrap';
import { NavLink as RRNavLink } from 'react-router-dom';
import { UserService } from "../auth/UserServices";
class Userbar extends Component {

  displayOption = () => {
    let customerId = localStorage.getItem('customerId');
    let userId = localStorage.getItem('userId');
    let token = localStorage.getItem('token');

    if (customerId && token) {
      return <Nav>
        <NavItem>
          <NavLink to="/customers/address" tag={RRNavLink}>Shipping Address</NavLink>
        </NavItem>
        <NavItem>
          <NavLink to="/customers/orders" tag={RRNavLink}>Order</NavLink>
        </NavItem>
        <NavItem>
          <NavLink to="/customers/information/update" tag={RRNavLink}>User Information</NavLink>
        </NavItem>
        <NavItem>
          <NavLink to="/" tag={RRNavLink} onClick={UserService.logout}>Logout</NavLink>
        </NavItem>
      </Nav>
    } else if (userId && token) {
      return <Nav>
        <NavItem>
          <NavLink to="/admin/products" tag={RRNavLink}>Admin Panel</NavLink>
        </NavItem>
        <NavItem>
          <NavLink to="/" tag={RRNavLink} onClick={UserService.adminLogout}>Logout</NavLink>
        </NavItem>
      </Nav>
    } else {
      return <Nav>
        <NavItem>
          <NavLink to="/customers/signin" tag={RRNavLink}>Sign In</NavLink>
        </NavItem>
        <NavItem>
          <NavLink to="/customers/signup" tag={RRNavLink}>Sign Up</NavLink>
        </NavItem>
      </Nav>
    }
  }

  render() {

    return (
      <div>
        {this.displayOption()}
      </div>
    );
  }
}

export { Userbar };
