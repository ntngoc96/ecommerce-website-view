import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.min.css';

import AdminLogin from './pages/AdminLogin';
import UploadProduct from './pages/UploadProduct';

import { Container } from 'reactstrap';

//Pages
import Home from './pages/Home';
import Address from "./pages/Address";
import Cart from "./pages/Cart";
import CustomerLogin from "./pages/CustomerLogin";
import Payment from "./pages/Payment";
import ManageProduct from "./pages/ManageProduct";
import EditProduct from "./pages/EditProduct";
import Category from "./pages/Category";
import EditCategory from "./pages/EditCategory";
import UpdateInformation from "./pages/UpdateInformation";
import ProductPage from "./pages/ProductPage";
import Order from "./pages/Order";
import AdminBar from "./components/AdminBar";
import ManageOrder from "./pages/ManageOrder";
import ManageCustomer from "./pages/ManageCustomer";
import ContinuePaymentOrder from "./pages/ContinuePaymentOrder";
import CustomerRegister from "./pages/CustomerRegister";
import CustomerAddress from "./pages/CustomerAddress";


//Components
import { Navbar } from "./components/Navbar";
import { Userbar } from "./components/Userbar";


//import CSS
import './App.css';

import { PrivateRoute, PrivateAdminRoute } from "./auth/PrivateRoute";



export default function App() {
  return (
    <Router>
      <Container>
        <div>
          <header className="d-flex flex-column align-items-end">
            <Userbar />
            <Navbar />
          </header>
          <main>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/products" component={ProductPage} />
              <Route path="/auth/admin" component={AdminLogin} />
              <Route path="/customers/signup" component={CustomerRegister} />
              <Route exact path="/customers/signin" component={CustomerLogin} />
              <Route path="/carts" component={Cart} />
              <PrivateRoute path="/checkout/address" component={Address} />
              <PrivateRoute path="/checkout/payment" component={Payment} />
              <PrivateRoute path="/customers/payment" component={ContinuePaymentOrder} />
              <PrivateAdminRoute exact path="/admin/products/upload">
                <div>
                  <AdminBar />
                  <UploadProduct />
                </div>
              </PrivateAdminRoute>
              <PrivateAdminRoute exact path="/admin/products">
                <div>
                  <AdminBar />
                  <ManageProduct />
                </div>
              </PrivateAdminRoute>
              <PrivateAdminRoute exact path="/admin/categories" >
                <div>
                  <AdminBar />
                  <Category />
                </div>
              </PrivateAdminRoute>
              <PrivateAdminRoute exact path="/admin/orders" >
                <div>
                  <AdminBar />
                  <ManageOrder />
                </div>
              </PrivateAdminRoute>
              <PrivateAdminRoute exact path="/admin/customers" >
                <div>
                  <AdminBar />
                  <ManageCustomer />
                </div>
              </PrivateAdminRoute>
              <PrivateAdminRoute path="/admin/products/:productId" component={EditProduct} />
              <PrivateAdminRoute path="/admin/categories/:categoryId" component={EditCategory} />
              <PrivateRoute path="/customers/information/update" component={UpdateInformation} />
              <PrivateRoute path="/customers/orders" component={Order} />
              <PrivateRoute path="/customers/address" component={CustomerAddress} />
            </Switch>
          </main>
          <footer className="footer my-4">

          </footer>
        </div>
      </Container>
    </Router>
  );
}




