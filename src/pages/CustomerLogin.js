import React, { Component } from 'react';
import Axios from "axios";
import classNames from 'classnames';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { UserService } from '../auth/UserServices';

class CustomerLogin extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: ''
    };
  }

  handleInputChange = (event) => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  }
  onSubmit = async (event) => {
    try {
      event.preventDefault();
      const { username, password } = this.state;
      let response = await Axios.post('/api/v1/customers/login', {
        username,
        password
      })


      if (response.status === 200) {
        UserService.adminLogout();
        localStorage.setItem('customerId', response.data.customerId);
        localStorage.setItem('token', response.data.token);
        this.props.history.push('/');
        window.location.reload();
      }
    } catch (error) {
      console.log(error);
      alert('Failure to loggin');
    }

  }
  render() {
    const { username, password } = this.state;
    return (
      <div className={classNames('d-flex', 'flex-column', 'align-items-center')}>
        <h1>Sign In</h1>
        <Form onSubmit={e => this.onSubmit(e)} className={classNames('w-50')}>
          <FormGroup>
            <Label for="username" className={classNames('font-weight-bold')}>Username</Label>
            <Input type="text" name="username" id="username" placeholder=""
              value={username}
              onChange={this.handleInputChange}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for="password" className={classNames('font-weight-bold')}>Password</Label>
            <Input type="password" name="password" id="password"
              value={password}
              onChange={this.handleInputChange}
              required
            />
          </FormGroup>
          <Button>Sign In</Button>
        </Form>
      </div>
    );
  }
}

export default CustomerLogin;
