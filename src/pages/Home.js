import React, { Component } from 'react';
import Axios from 'axios';
import { Form, Input, Button } from 'reactstrap';

import ListProducts from '../components/ListProducts';
import Pagination from '../components/Pagination';


class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Items: [],
      currentPage: 1,
      itemsPerPage: 8,
      searchValue: ''
    }
  }

  handleInputChange = (event) => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  }

  onSubmit = async (event) => {
    event.preventDefault();
    try {
      const { searchValue } = this.state;
      let response = await Axios.get(`/api/v1/products/filter/name?q=${searchValue}`)

      if (response.status === 200) {
        let Items = response.data;
        this.setState((state, props) => { return { Items,currentPage:1 } });
      } else {
        const error = new Error(response.error);
        throw error;
      }
    } catch (error) {
      console.log(error);
      alert('Error logging in please try again');
    }
  }

  paginate = pageNumber => this.setState((state, props) => { return { currentPage: pageNumber } });



  async componentDidMount() {
    try {
      let response = await Axios.get('/api/v1/products?offset=0&limit=1000&by=name&type=ASC')
      let { data: Items } = response;

      this.setState((state, props) => { return { Items } });
    } catch (error) {
      console.log(error);
      alert('Failure to load product');
    }
  }



  render() {
    let { Items, searchValue, itemsPerPage, currentPage } = this.state;

    // Get current posts
    const indexOfLastItems = currentPage * itemsPerPage;
    const indexOfFirstItems = indexOfLastItems - itemsPerPage;
    const currentItems = Items.slice(indexOfFirstItems, indexOfLastItems);

    return (
      <div>
        <Form onSubmit={e => this.onSubmit(e)} className="d-flex ">
          <Input
            name='searchValue'
            value={searchValue}
            onChange={this.handleInputChange}

          />
          <Button>Search</Button>
        </Form>
        <ListProducts Items={currentItems} />
        <Pagination
          itemsPerPage={itemsPerPage}
          totalItems={Items.length}
          paginate={this.paginate}
        />
      </div>
    );
  }
}

export default Home;