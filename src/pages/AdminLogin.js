import React, { Component } from 'react';
import Axios from 'axios';
import classNames from 'classnames';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { UserService } from '../auth/UserServices';


export default class AdminLogin extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: ''
    };
  }
  handleInputChange = (event) => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  }
  onSubmit = (event) => {
    const { username, password } = this.state;
    event.preventDefault();
    Axios.post('/auth/v1/admin', {
      username,
      password
    })
      .then((res) => {
        console.log(res.status);

        if (res.status === 200) {
          UserService.logout();
          localStorage.setItem('token',res.data.token);
          localStorage.setItem('userId',res.data.userName);
          this.props.history.push('/admin/products');
          window.location.reload();
        } else {
          const error = new Error(res.error);
          throw error;
        }
      })
      .catch((err) => {
        console.log(err);
        alert('Error logging in please try again');
      });
  }
  render() {
    let { username, password } = this.state;

    return (
      <div className={classNames('d-flex', 'flex-column', 'align-items-center')}>
        <h1>Admin Login</h1>
        <Form onSubmit={e => this.onSubmit(e)} className={classNames('w-50')}>
          <FormGroup>
            <Label for="username" className={classNames('font-weight-bold')}>Username</Label>
            <Input type="text" name="username" id="username" placeholder=""
              value={username}
              onChange={this.handleInputChange}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for="password" className={classNames('font-weight-bold')}>Password</Label>
            <Input type="password" name="password" id="password"
              value={password}
              onChange={this.handleInputChange}
              required
            />
          </FormGroup>
          <Button>Login</Button>
        </Form>
      </div>
    );
  }
}