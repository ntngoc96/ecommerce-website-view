import React, { Component } from 'react';
import Axios from 'axios';
import classNames from 'classnames';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { NavLink } from "react-router-dom";

const config = {
  onUploadProgress: progressEvent => console.log(Math.round(progressEvent.loaded / progressEvent.total) * 100)
}
class EditProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      producer: '',
      unitPrice: '',
      discount: 0,
      categories: [],
      categoryId: '',
      description: '',
      star: 0,
      productImages: [],
      files: [],

    }
  }

  onInputChange = (e) => {
    let { value, name } = e.target;
    this.setState((state, props) => { return { [name]: value } });
  }


  onSubmit = async (e) => {
    try {
      e.preventDefault();

      let { name, producer, unitPrice, discount, categoryId, description, star } = this.state;
      let { productId } = this.props.match.params;
      let response = await Axios.patch(`/api/v1/products/${productId}`, {
        name,
        producer,
        unitPrice,
        discount,
        categoryId,
        description,
        star
      }, config);

      if (response.status === 200) {
        alert('Update product successfully');
        this.props.history.push('/admin/products');
      }
    } catch (error) {
      console.log(error);
      alert('Update error, Try again')
    }

  }

  onDeleteClicked = async (productImageId) => {
    try {
      let response = await Axios.delete(`/api/v1/products/images/${productImageId}`, config);

      if (response.status === 200) {
        window.location.reload();
      }
    } catch (error) {
      console.log(error);
      alert('Delete error, Try again')
    }

  }

  async componentDidMount() {
    try {
      const { productId } = this.props.match.params;

      let [responseCategories, responseProduct] = await Promise.all([
        Axios.get('/api/v1/categories'),
        Axios.get(`/api/v1/products/${productId}`)
      ]);

      let { name, producer, categoryId, description, discount, star, unitPrice, productImages } = responseProduct.data;

      const categories = responseCategories.data;

      this.setState((state, props) => { return { name, producer, categoryId, description, discount, star, unitPrice, categories, productImages } });


    }
    catch (err) {
      console.log(err);
      alert('Error edit please try again');
    };
  }

  onFilesChange = (e) => {
    this.setState({ files: [...this.state.files, ...e.target.files] })
  }

  onImageSubmit = async () => {
    try {
      const { productId } = this.props.match.params;
      const data = new FormData();
      const { files } = this.state;

      for (const iterator of files) {
        data.append('files', iterator)
      }

      let response = await Axios.post(`/api/v1/products/${productId}/images`, data, config);
      if (response.status === 200) {
        alert("Update product image successfully");
        window.location.reload();
      }
    } catch (error) {
      console.log(error);
      alert("Upload image error")
    }
  }

  render() {
    let { name, producer, unitPrice, discount, categories, categoryId, productImages, description, star } = this.state;

    return (
      <div>
        <NavLink to="/admin/categories"><i class="fas fa-backward">Back</i></NavLink>
        <h1 className="text-center">Edit Product</h1>
        <Form onSubmit={e => { this.onSubmit(e) }}>
          <FormGroup>
            <Label for="name">Product name</Label>
            <Input type="text" name="name" id="name"
              value={name}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. IphoneX, Samsung Galaxy Note 10" />
          </FormGroup>
          <FormGroup>
            <Label for="producer">Producer</Label>
            <Input type="text" name="producer" id="producer"
              value={producer}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. Apple, Samsung" />
          </FormGroup>
          <FormGroup>
            <Label for="unitPrice">Product Price</Label>
            <Input type="number" name="unitPrice" id="unitPrice"
              value={unitPrice}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. 100, 200, 500" />
          </FormGroup>
          <FormGroup>
            <Label for="discount">Discount (percentage)</Label>
            <Input type="number" name="discount" id="discount"
              value={discount}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. 5, 10, 20" />
          </FormGroup>
          <FormGroup>
            <Label for="star">Star </Label>
            <Input type="number" name="star" id="star"
              value={star}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. 1,2,3,4,5" />
          </FormGroup>
          <FormGroup>
            <Label for="description">Description </Label>
            <Input type="text" name="description" id="description"
              value={description}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. Describe product information" />
          </FormGroup>
          <FormGroup>
            <Label for="categoryId">Category</Label>
            <Input type="select" name="categoryId" id="categoryId"
              value={categoryId}
              onChange={e => this.onInputChange(e)}
            >
              {categories.map((value, index) => <option key={value.categoryId} value={value.categoryId}>{value.name}</option>)}
            </Input>
          </FormGroup>
          <Button >Submit</Button>
          <FormGroup>
            <h2>Update Product Images</h2>
            {productImages.map((value, index) => <div key={value.productImageId}>
              <img alt="productimg" className={classNames('w-20r')} src={value.imageUrl}  />
              <Button onClick={e => this.onDeleteClicked(value.productImageId)}>Delete</Button>
            </div>)}
          </FormGroup>
          <FormGroup>
            <div><h2>Upload multiple images</h2></div>
            <input type="file" multiple onChange={this.onFilesChange} />
            <Button onClick={this.onImageSubmit}>Upload Image</Button>
          </FormGroup>
          
        </Form>
      </div>
    )
  }
}

export default EditProduct;