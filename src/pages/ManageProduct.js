import React, { Component } from 'react';
import Axios from 'axios';
import classnames from 'classnames';
import {
  Row, Spinner, TabContent, TabPane, Nav, NavItem, NavLink, Form, Input, Button
} from 'reactstrap';
import { NavLink as RRNavLink } from "react-router-dom";

import Product from '../components/Product';
import Pagination from '../components/Pagination';

class ManageProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Items: [],
      Categories: [],
      activeTab: '1',
      currentPage: 1,
      itemsPerPage: 8,

    }
  }
  toggle = tab => {
    if (this.state.activeTab !== tab) this.setState((state, props) => { return { activeTab: tab, currentPage: 1 } });
  }

  paginate = pageNumber => this.setState((state, props) => { return { currentPage: pageNumber } });

  onDeleteClicked = async (productId) => {
    try {
      let response = await Axios.delete(`/api/v1/products/${productId}`);
      if (response.status === 200) {
        alert('Delete Successfully');
        window.location.reload();


      }

    } catch (error) {
      console.log(error);
      alert("Delete product error")
    }
  }

  handleInputChange = (event) => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  }

  onSubmit = async (event) => {
    event.preventDefault();
    try {
      const { searchValue } = this.state;
      let response = await Axios.get(`/api/v1/products/filter/name?q=${searchValue}`)

      if (response.status === 200) {
        let Items = response.data;
        this.setState((state, props) => { return { Items } });
      } else {
        const error = new Error(response.error);
        throw error;
      }
    } catch (error) {
      console.log(error);
      alert('Error logging in please try again');
    }
  }

  async componentDidMount() {
    try {
      let [responseCategories, responseProduct] = await Promise.all([
        Axios.get(`/api/v1/categories`),
        Axios.get(`/api/v1/products?offset=0&limit=100&by=name&type=ASC`),
      ]);

      if (responseCategories.status === 200 && responseProduct.status === 200) {
        let Items = responseProduct.data;
        let Categories = responseCategories.data;
        this.setState((state, props) => { return { Items, Categories } });

      }

    } catch (error) {
      console.log(error);
      alert("Loading product error")
    }
  }


  render() {
    let { Items, activeTab, Categories, searchValue, itemsPerPage, currentPage } = this.state;

    // Get current posts
    const indexOfLastItems = currentPage * itemsPerPage;
    const indexOfFirstItems = indexOfLastItems - itemsPerPage;
    //filter items each tab
    let currentFilterItems = [];
    let ItemsTemp = [];
    for (let index = 0; index < Items.length; index++) {
      if (Items[index].categoryId === Categories[activeTab - 1].categoryId) {
        ItemsTemp.push(Items[index]);
      }
    }
    currentFilterItems = ItemsTemp.slice(indexOfFirstItems, indexOfLastItems);

    return (
      <div>
        <NavLink className="btn btn-primary w-25" to="/admin/products/upload" tag={RRNavLink}>Upload new product</NavLink>
        <Form onSubmit={e => this.onSubmit(e)} className="d-flex ">
          <Input
            name='searchValue'
            value={searchValue}
            onChange={this.handleInputChange}

          />
          <Button>Search</Button>
        </Form>
        <Nav tabs>
          {Categories.length !== 0 && Categories.map((value, index) =>
            <NavItem key={index}>
              <NavLink
                className={classnames({ active: activeTab === `${index + 1}` })}
                onClick={() => { this.toggle(`${index + 1}`); }}
              >
                {value.name}
              </NavLink>
            </NavItem>)}
        </Nav>
        <TabContent activeTab={activeTab}>
          {Categories.length !== 0 && Categories.map((category, index) =>
            <TabPane tabId={`${index + 1}`} key={index}>
              {currentFilterItems.length === 0 && <Spinner color="primary" />}
              {currentFilterItems.length !== 0 && <Row>
                {currentFilterItems.map((item, index) =>
                  <Product key={index} type="edit-mode" {...item}
                    onDeleteClicked={this.onDeleteClicked} />)}
              </Row>}
              <Pagination
                itemsPerPage={itemsPerPage}
                totalItems={ItemsTemp.length}
                paginate={this.paginate}
              />
            </TabPane>)}
        </TabContent>
      </div>
    );
  }
}

export default ManageProduct;
