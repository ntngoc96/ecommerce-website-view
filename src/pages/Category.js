import React, { Component } from 'react';
import Axios from 'axios';
import classNames from 'classnames';
import { Spinner, Table, Button, Form, FormGroup, Label, Input } from "reactstrap";
import { NavLink } from "react-router-dom";
class Category extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: [],
      name: '',
      order: 0,
    }
  }

  async componentDidMount() {
    try {
      let response = await Axios.get('/api/v1/categories')
      if (response.status === 200) {
        const { data: categories } = response;
        this.setState((state, props) => { return { categories } });
      }
    } catch (error) {
      console.log(error);
      alert('Loading categiries error');
    }
  }

  onDeleteClicked = async (categoryId) => {
    try {
      let response = await Axios.delete(`/api/v1/categories/${categoryId}`);
      if (response.status === 200) {
        alert('Delete Successfully');
        window.location.reload();
      }
    } catch (error) {
      console.log(error);
      alert('Delete error, Try again');
    }
  }

  onInputChange = (e) => {
    let { value, name } = e.target;
    this.setState((state, props) => { return { [name]: value } });
  }

  onSubmit = async (e) => {
    try {
      e.preventDefault();
      let { name, order } = this.state;

      let response = await Axios.post(`/api/v1/categories`, {
        name, order
      });
      if (response.status === 201) {
        alert('Add successfully');
        this.setState((state, props) => { return { name: '', order: 0 } });
        window.location.reload();
      }
    } catch (error) {
      console.log(error);
      alert('Failure to add category');
    }
  }

  render() {
    let { categories, name, order } = this.state;

    return (
      <div>
        {categories.length === 0 && <Spinner color="primary" />}
        {categories.length !== 0 && <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>Category Id</th>
              <th>Category Name</th>
              <th>Order</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{categories.map((value, index) =>
            <tr key={value.categoryId}>
              <th scope="row">{index}</th>
              <td>{value.categoryId}</td>
              <td>{value.name}</td>
              <td>{value.order}</td>
              <td>
                <NavLink className={classNames('btn', 'btn-primary', 'w-40', 'mx-2')} to={`/admin/categories/${value.categoryId}`} >Edit</NavLink>
                <Button onClick={e => this.onDeleteClicked(value.categoryId)}>Delete</Button>
              </td>
            </tr>
          )}
          </tbody>
        </Table>}
        <h2>Add Category</h2>
        <Form >
          <FormGroup>
            <Label for="name">Category name</Label>
            <Input type="text" name="name" id="name"
              value={name}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. Laptop, Smartphone" />
          </FormGroup>
          <FormGroup>
            <Label for="order">Category Order</Label>
            <Input type="text" name="order" id="order"
              value={order}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. 1,2,3,4,..." />
          </FormGroup>
          <Button onClick={this.onSubmit}>Upload</Button>
        </Form>
      </div>
    );
  }
}

export default Category;