import React, { Component } from 'react';
import Axios from 'axios';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

const config = {
  onUploadProgress: progressEvent => console.log(Math.round(progressEvent.loaded / progressEvent.total) * 100)
}
class UpdateInformation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      address: '',
      dob: '',
      phonenumber: 0,
      creditCard: '',
      files: '',
      avatar: ''
    }
  }

  onInputChange = (e) => {
    let { value, name } = e.target;
    this.setState((state, props) => { return { [name]: value } });
  }


  onSubmit = async (e) => {
    try {
      e.preventDefault();

      let { name, address, dob, phonenumber, creditCard } = this.state;
      let customerId = localStorage.getItem('customerId');
      let response = await Axios.patch(`/api/v1/customers/${customerId}/personal-info`, {
        name,
        address,
        dob,
        phonenumber,
        creditCard,
      }, config);

      if (response.status === 200) {
        alert('Update information successfully')
      }
    } catch (error) {
      console.log(error);
      alert('Update error, Try again')
    }

  }


  async componentDidMount() {
    try {
      const customerId = localStorage.getItem('customerId')

      let response = await Axios.get(`/api/v1/customers/${customerId}`)

      let { name, address, dob, phonenumber, creditCard, avatar } = response.data;


      this.setState((state, props) => { return { name, address, dob, phonenumber, creditCard, avatar } });

    }
    catch (err) {
      console.log(err);
      alert('Error loading please try again');
    };
  }

  onFilesChange = (e) => {
    this.setState({ files: e.target.files[0] })
  }

  onImageSubmit = async () => {
    try {
      const customerId = localStorage.getItem('customerId');

      const data = new FormData();
      const { files } = this.state;

      data.append('avatar', files);

      let response = await Axios.patch(`/api/v1/customers/${customerId}/avatar`, data, config);

      if (response.status === 200) {
        alert("Update avatar successfully");
        window.location.reload();
      }
    } catch (error) {
      console.log(error);
      alert("Upload avatar error")
    }
  }

  render() {
    let { name, address, dob, phonenumber, creditCard, avatar } = this.state;

    return (
      <div className="d-flex flex-column align-items-center p-2">
        <h1 className="text-center">Update Information</h1>
        <Form onSubmit={e => { this.onSubmit(e) }} className='w-50 '>
          <FormGroup>
            {
              <img className='w-20r h-20r' src={avatar} alt="avatar" />
            }
          </FormGroup>
          <FormGroup>
            <div><h2>Upload avatar</h2></div>
            <input type="file" onChange={this.onFilesChange} />
            <Button onClick={this.onImageSubmit}>Upload Avatar</Button>
          </FormGroup>
          <FormGroup>
            <Label for="name">Full name</Label>
            <Input type="text" name="name" id="name"
              value={name}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. John Doe, Natasha Skonav" />
          </FormGroup>
          <FormGroup>
            <Label for="address">Address</Label>
            <Input type="text" name="address" id="address"
              value={address}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. Texas,New York City" />
          </FormGroup>
          <FormGroup>
            <Label for="dob">Dob</Label>
            <Input type="text" name="dob" id="dob"
              value={dob}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. ISO: 1990-10-11,2001-09-12" />
          </FormGroup>
          <FormGroup>
            <Label for="phonenumber">Phonenumber</Label>
            <Input type="text" name="phonenumber" id="phonenumber"
              value={phonenumber}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. +1 965 463 1922" />
          </FormGroup>
          <FormGroup>
            <Label for="creditCard">Credit Card </Label>
            <Input type="text" name="creditCard" id="creditCard"
              value={creditCard}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. 4242 4242 4242 4242" />
          </FormGroup>
          <Button >Submit</Button>
        </Form>
      </div>
    )
  }
}

export default UpdateInformation;