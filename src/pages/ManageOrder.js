import React, { Component } from 'react';
import Axios from 'axios';
import {
  Table, Spinner, Button, Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

class ManageOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Orders: [],
      OrderDetails: [],
      modal: false
    }
  }

  toggle = async () => {
    let { modal } = this.state;
    this.setState((state, props) => { return { modal: !modal } });
  }

  onDetailClicked = async (orderId) => {
    try {
      let { modal } = this.state;
      let response = await Axios.get(`/api/v1/orders/${orderId}/details`);
      if (response.status === 200) {
        this.setState((state, props) => { return { modal: !modal, OrderDetails: response.data.orderDetails } });
      }

    } catch (error) {
      console.log(error);
      alert("Display product detail error")
    }
  }

  discountUnitPrice = (order) => {
    let { orderDetails } = order;
    let amount = 0;
    for (let index = 0; index < orderDetails.length; index++) {
      amount += (orderDetails[index].unitPrice - (orderDetails[index].unitPrice * orderDetails[index].discount) / 100)* orderDetails[index].quantity
      amount = Math.round(amount*1000)/1000;
    }

    return amount;
  }

  async componentDidMount() {
    try {
      let response = await Axios.get('/api/v1/orders?offset=0&limit=50')
      if (response.status === 200) {
        const { data: Orders } = response;
        this.setState((state, props) => { return { Orders } });
      }
    } catch (error) {
      console.log(error);
      alert('Loading orders error');
    }

  }
  render() {
    let { Orders, modal, OrderDetails } = this.state;
    let className = "";

    return (
      <div>
        <div>
          <Modal isOpen={modal} toggle={this.toggle} className={className}>
            <ModalHeader >List Order Detail</ModalHeader>
            <ModalBody>
              <Table size="sm">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Order Detail Id</th>
                    <th>Product Id</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Discount</th>
                  </tr>
                </thead>
                <tbody>
                  {OrderDetails.length !== 0 && OrderDetails.map((value, index) =>
                    <tr>
                      <th scope="row">{index}</th>
                      <td>{value.orderDetailId}</td>
                      <td>{value.productId}</td>
                      <td>{value.quantity}</td>
                      <td>{value.unitPrice}</td>
                      <td>{value.discount}</td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={this.toggle}>Close</Button>
            </ModalFooter>
          </Modal>
        </div>

        <h1>Order History</h1>
        {Orders.length === 0 && <Spinner color="primary" />}
        {Orders.length !== 0 && <Table >
          <thead>
            <tr>
              <th>#</th>
              <th>Order Id</th>
              <th>Date Order</th>
              <th>Customer Id</th>
              <th>Payment Id</th>
              <th>Message</th>
              <th>Order Status</th>
              <th>Total</th>
              <th>User Deleted</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {Orders.map((value, index) => <tr key={index}>
              <th scope="row">{index + 1}</th>
              <td>{value.orderId}</td>
              <td>{value.dateOrder.slice(0, 19)}</td>
              <td>{value.customerId}</td>
              <td>{value.paymentId}</td>
              <td>{value.message}</td>
              <td>{value.orderStatus === 1 ? 'paid' : 'unpaid'}</td>
              <td>{this.discountUnitPrice(value)}</td>
              <td>{value.isDeleted === 1 ? 'true': 'false'}</td>
              <td><div className="d-flex flex-row">
                <Button className="btn-success" onClick={e => this.onDetailClicked(value.orderId)}>Detail</Button>
              </div></td>
            </tr>)}
          </tbody>
        </Table>}
      </div>
    );
  }
}

export default ManageOrder;