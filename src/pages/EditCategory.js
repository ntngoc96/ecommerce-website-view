import React, { Component } from 'react';
import Axios from 'axios';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { NavLink } from "react-router-dom";

const config = {
  onUploadProgress: progressEvent => console.log(Math.round(progressEvent.loaded / progressEvent.total) * 100)
}


class EditCategory extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      order: 0,

    }
  }

  onInputChange = (e) => {
    let { value, name } = e.target;
    this.setState((state, props) => { return { [name]: value } });
  }


  onSubmit = async (e) => {
    try {
      e.preventDefault();

      let { name, order } = this.state;
      let { categoryId } = this.props.match.params;
      let response = await Axios.patch(`/api/v1/categories/${categoryId}`, {
        name,
        order
      }, config);

      if (response.status === 200) {
        alert('Update Category successfully');
        this.props.history.push('/admin/categories')
      }
    } catch (error) {
      console.log(error);
      alert('Update error, Try again')
    }

  }


  async componentDidMount() {
    try {
      const { categoryId } = this.props.match.params;

      let response = await Axios.get(`/api/v1/categories/${categoryId}`);

      let { name, order } = response.data;


      this.setState((state, props) => { return { name, order } });


    }
    catch (err) {
      console.log(err);
      alert('Error edit please try again');
    };
  }


  render() {
    let { name, order } = this.state;

    return (
      <div>
      <NavLink to="/admin/categories"><i class="fas fa-backward">Back</i></NavLink>
        <h1 className="text-center">Edit Category</h1>
        <Form onSubmit={e => { this.onSubmit(e) }}>
          <FormGroup>
            <Label for="name">Category name</Label>
            <Input type="text" name="name" id="name"
              value={name}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. Laptop,Smartphone" />
          </FormGroup>
          <FormGroup>
            <Label for="order">Order</Label>
            <Input type="number" name="order" id="order"
              value={order}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. 1,2,3,4" />
          </FormGroup>
          <Button >Submit</Button>
        </Form>
      </div>
    )
  }
}

export default EditCategory;