import React, { Component } from 'react';
import Axios from 'axios';
import Product from '../components/Product';
import classnames from 'classnames';
import {
  Row, Spinner, TabContent, TabPane, Nav, NavItem, NavLink, Form, Input, Button,
  ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem
} from 'reactstrap';
import Pagination from '../components/Pagination';



class ProductPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Items: [],
      Categories: [],
      activeTab: '1',
      searchValue: '',
      currentPage: 1,
      itemsPerPage: 8,
      dropdownOpen: false
    }

  }

  onAddToCartClicked = (value) => Axios.get(`/api/v1/carts/${value.productId}`);


  toggle = tab => {
    if (this.state.activeTab !== tab) this.setState((state, props) => { return { activeTab: tab, currentPage: 1 } });
  }

  toggleSort = () => this.setState((state, props) => { return { dropdownOpen: !this.state.dropdownOpen } });
  ;

  paginate = pageNumber => this.setState((state, props) => { return { currentPage: pageNumber } });


  handleInputChange = (event) => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  }


  onItemClicked = async (value) => {
    try {
      await Axios.get(`/api/v1/carts/${value.productId}`);
    } catch (error) {
      alert('Add to cart error');
    }
  }

  onSortChange = async (column, type) => {
    try {
      let response = await Axios.get(`/api/v1/products?offset=0&limit=1000&by=${column}&type=${type}`);
      if (response.status === 200) {
        this.setState((state, props) => { return { Items: response.data } });
      }
    } catch (error) {
      console.log(error);
      alert('sort error try again');
    }


  }

  onSubmit = async (event) => {
    event.preventDefault();
    try {
      const { searchValue } = this.state;
      let response = await Axios.get(`/api/v1/products/filter/name?q=${searchValue}`)

      if (response.status === 200) {
        let Items = response.data;
        this.setState((state, props) => { return { Items } });
      } else {
        const error = new Error(response.error);
        throw error;
      }
    } catch (error) {
      console.log(error);
      alert('Error logging in please try again');
    }
  }


  async componentDidMount() {
    try {
      let [responseCategories, responseProduct] = await Promise.all([
        Axios.get(`/api/v1/categories`),
        Axios.get(`/api/v1/products?offset=0&limit=1000&by=name&type=ASC`),
      ]);

      if (responseCategories.status === 200 && responseProduct.status === 200) {
        let Items = responseProduct.data;
        let Categories = responseCategories.data;
        this.setState((state, props) => { return { Items, Categories } });
      }

    } catch (error) {
      console.log(error);
      alert("Loading product error")
    }
  }

  render() {
    let { Items, activeTab, Categories, searchValue, itemsPerPage, currentPage, dropdownOpen } = this.state;

    // Get current posts
    const indexOfLastItems = currentPage * itemsPerPage;
    const indexOfFirstItems = indexOfLastItems - itemsPerPage;
    const currentItems = Items.slice(indexOfFirstItems, indexOfLastItems);
    //filter items each tab
    let currentFilterItems = [];
    let ItemsTemp = [];
    //next if activetab is all
    if (activeTab > 1) {
      for (let index = 0; index < Items.length; index++) {
        if (Items[index].categoryId === Categories[activeTab - 2].categoryId) {
          ItemsTemp.push(Items[index]);
        }
      }
      currentFilterItems = ItemsTemp.slice(indexOfFirstItems, indexOfLastItems);
    }

    return (
      <div>
        <Form onSubmit={e => this.onSubmit(e)} className="d-flex ">
          <Input
            name='searchValue'
            value={searchValue}
            onChange={this.handleInputChange}

          />
          <Button>Search</Button>
        </Form>
        <ButtonDropdown isOpen={dropdownOpen} toggle={this.toggleSort}>
          <DropdownToggle caret>
            Sort by
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem onClick={() => this.onSortChange('unitPrice', 'ASC')}>Price - ASC</DropdownItem>
            <DropdownItem onClick={() => this.onSortChange('unitPrice', 'DESC')}>Price - DESC </DropdownItem>
            <DropdownItem divider />
            <DropdownItem onClick={() => this.onSortChange('discount', 'ASC')}>Discount - ASC</DropdownItem>
            <DropdownItem onClick={() => this.onSortChange('discount', 'DESC')}>Discount - DESC </DropdownItem>
          </DropdownMenu>
        </ButtonDropdown>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === `1` })}
              onClick={() => { this.toggle(`1`); }}
            >
              All
            </NavLink>
          </NavItem>
          {Categories.length !== 0 && Categories.map((value, index) =>
            <NavItem key={index}>
              <NavLink
                className={classnames({ active: activeTab === `${index + 2}` })}
                onClick={() => { this.toggle(`${index + 2}`); }}
              >
                {value.name}
              </NavLink>
            </NavItem>)}
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId={`1`}>
            {Items.length === 0 && <Spinner color="primary" />}
            {Items.length !== 0 && <Row>
              {currentItems.map((item, index) =>
                <Product key={index} type="view-mode" {...item}
                  onAddToCartClicked={this.onItemClicked} />)}
            </Row>}
            <Pagination
              itemsPerPage={itemsPerPage}
              totalItems={Items.length}
              paginate={this.paginate}
            />
          </TabPane>
          {Categories.length !== 0 && Categories.map((category, index) =>
            <TabPane tabId={`${index + 2}`} key={index}>
              {currentFilterItems.length === 0 && <Spinner color="primary" />}
              {currentFilterItems.length !== 0 && <Row>
                {currentFilterItems.map((item, index) =>
                  <Product key={index} type="view-mode" {...item}
                    onAddToCartClicked={this.onItemClicked} />)}
              </Row>}
              <Pagination
                itemsPerPage={itemsPerPage}
                totalItems={ItemsTemp.length}
                paginate={this.paginate}
              />
            </TabPane>)}
        </TabContent>
      </div>
    );
  }
}

export default ProductPage;