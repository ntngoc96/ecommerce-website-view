import React, { Component } from 'react';
import Axios from 'axios';
import {
  Table, Spinner,
} from 'reactstrap';

class ManageCustomer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      CustomersInfo: [],
      modal: false
    }

  }

  async componentDidMount() {
    try {
      let response = await Axios.get(`/api/v1/customers`);
  
      this.setState((state, props) => { return { CustomersInfo: response.data } });
    } catch (error) {
      alert('Error to loading customers')
    }
  }


  render() {
    let { CustomersInfo } = this.state;

    return (
      <div>

        <h1>Customer Account</h1>
        {CustomersInfo.length === 0 && <Spinner color="primary" />}
        {CustomersInfo.length !== 0 && <Table >
          <thead>
            <tr>
              <th>#</th>
              <th>Account Id</th>
              <th>Email</th>
              <th>Avatar</th>
              <th>Full Name</th>
              <th>Address</th>
              <th>Dob</th>
              <th>Phone </th>
              <th>Credit Card</th>
              <th>Customer Id</th>
              <th>isBlocked</th>
            </tr>
          </thead>
          <tbody>
            {CustomersInfo.map((value, index) => <tr key={index}>
              <th scope="row">{index + 1}</th>
              <td>{value.accountId}</td>
              <td>{value.email}</td>
              <td>{value.customer ? <img className="w-10r" src={value.customer.avatar} alt="avatar" /> : ""}</td>
              <td>{value.customer ? value.customer.name : ""}</td>
              <td>{value.customer ? value.customer.address : ""}</td>
              <td>{value.customer ? value.customer.dob : ""}</td>
              <td>{value.customer ? value.customer.phonenumber : ""}</td>
              <td>{value.customer ? value.customer.creditCard : ""}</td>
              <td>{value.customer ? value.customer.customerId : ""}</td>
              <td>{value.customer ? value.customer.isBlocked : ""}</td>
            </tr>)}
          </tbody>
        </Table>
        }
      </div>
    );
  }
}

export default ManageCustomer;