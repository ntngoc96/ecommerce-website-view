import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Axios from 'axios';

class Address extends Component {
  constructor(props) {
    super(props);

    this.state = {
      addresses: [],
      name: '',
      phonenumber: '',
      address: '',
      addressId: '',
    }
  }

  componentDidMount() {
    const customerId = localStorage.getItem('customerId')
    Axios.get(`/api/v1/customers/${customerId}/address`).then((response) => {
      console.log(response);

      if (response.status === 200) {
        let addresses = response.data;
        this.setState((state, props) => { return { addresses } });

      } else {
        const error = new Error(response.error);
        throw error;
      }
    })
      .catch((error) => {
        console.log(error);
        alert('No address found, Enter new address');
      });
  }

  handleInputChange = (event) => {
    const { value, name } = event.target;

    this.setState({
      [name]: value
    });
  }

  handleAddressChange = (event) => {
    const { value } = event.target;
    let temp = value.split('/');
    if (temp.length === 2) {
      let addressId = temp[0];
      let address = temp[1];
      this.setState({
        address,
        addressId
      });
    } else {
      let address = temp[0];
      this.setState({
        address,
        addressId: ''
      });
    }

    
  }

  onSubmit = async (event) => {
    try {
      event.preventDefault();

      const { name, phonenumber, address,addressId } = this.state;
      const customerId = localStorage.getItem('customerId');
      let response = await Axios.post(`/api/v1/customers/${customerId}/address`, {
        name,
        phonenumber,
        address,
        addressId
      });


      if (response.status === 200) {
        const { addressId } = response.data.addressResult;
        localStorage.setItem('addressId', addressId);
        response = await Axios.post(`/api/v1/carts/checkout/confirm`, {
          addressId,
          customerId
        });
        console.log(response);

        if (response.status === 201) {
          const { orderId } = response.data.order;
          localStorage.setItem('orderId', orderId);
          this.props.history.push('/checkout/payment');
        }
      }

    } catch (error) {
      console.log(error);
      alert("add address error")
    }
  }


  render() {
    const { addresses, name, phonenumber, address } = this.state;
    console.log(this.state);

    return (
      <div>
        <h1>Shipping Address</h1>
        <Form onSubmit={this.onSubmit} >
          <FormGroup>
            <Label for="name">Full name</Label>
            <Input type="text" name="name" id="name" placeholder="eg. John Doe, Marcus Harley"
              value={name}
              onChange={this.handleInputChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="phonenumber">Phone number</Label>
            <Input type="text" name="phonenumber" id="phonenumber" placeholder="eg. +1 617-863-8665"
              value={phonenumber}
              onChange={this.handleInputChange}
            />
          </FormGroup>
          <FormGroup>
            <Label for="addresses">Address</Label>
            <Input type="text" list="address" id="addresses" onChange={this.handleAddressChange} name="address" value={address} />
            <datalist id="address">
              {addresses.map((value, index) =>
                <option key={index} value={`${value.addressId}/${value.address}`} >{value.address}</option>
              )}
            </datalist>
          </FormGroup>
          <Button>Submit</Button>
        </Form>
      </div>
    );
  }
}

export default Address;