import React, { Component } from 'react';
import { Elements, StripeProvider } from 'react-stripe-elements';
import CheckoutForm from '../components/CheckoutForm';

class Payment extends Component {
  render() {
    
    return (
      <StripeProvider apiKey="pk_test_spKjWx5KUm89wQ7EOlpgiQs700Q9se4Khq">
        <div className="example">
          <Elements>
            <CheckoutForm />
          </Elements>
        </div>
      </StripeProvider>
    );
  }
}

export default Payment;