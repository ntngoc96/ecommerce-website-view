import React, { Component } from 'react';
import { Elements, StripeProvider } from 'react-stripe-elements';

import ContinuePaymentCheckoutForm from '../components/ContinuePaymentCheckoutForm';

class ContinuePaymentOrder extends Component {
  render() {
    
    return (
      <StripeProvider apiKey="pk_test_spKjWx5KUm89wQ7EOlpgiQs700Q9se4Khq">
        <div className="example">
          <Elements>
            <ContinuePaymentCheckoutForm />
          </Elements>
        </div>
      </StripeProvider>
    );
  }
}

export default ContinuePaymentOrder;