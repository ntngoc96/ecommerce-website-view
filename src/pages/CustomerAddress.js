import React, { Component } from 'react';
import Axios from 'axios';
import { Table,Spinner,Button } from "reactstrap";

class CustomerAddress extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Address: [],
    }

  }

  onDeleteAddressClicked = async (addressId) =>{
    try {
      let customerId = localStorage.getItem('customerId')
      let response = await Axios.delete(`/api/v1/customers/${customerId}/address/${addressId}`);
      if (response.status === 200) {
        window.location.reload();
      }
    } catch (error) {
      console.log(error);
      alert('Failure to delete address, Try again');
    }
  }

  async componentDidMount() {
    try {
      const customerId = localStorage.getItem('customerId');
      let response = await Axios.get(`/api/v1/customers/${customerId}/address`);
      if (response.status === 200) {
        this.setState((state, props) => { return { Address: response.data } });
      }
    } catch (error) {
      console.log(error);
      alert('Loading address error, Try again');
    }
  }

  render() {
    let { Address } = this.state;
    return (
      <div>
        <h1>Customer Address</h1>
        {Address.length === 0 && <Spinner color="primary" />}
        {Address.length !== 0 && <Table >
          <thead>
            <tr>
              <th>#</th>
              <th>Address Id</th>
              <th>Name</th>
              <th>Address</th>
              <th>Phonenumber</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {Address.map((value, index) => <tr key={index}>
              <th scope="row">{index + 1}</th>
              <td>{value.addressId}</td>
              <td>{value.name}</td>
              <td>{value.address}</td>
              <td>{value.phonenumber}</td>
              <td className="d-flex flex-row">
                <Button className="btn-danger" onClick={()=>this.onDeleteAddressClicked(value.addressId)}>Delete</Button>
              </td>
            </tr>)}
          </tbody>
        </Table>}
      </div>
    );
  }
}

export default CustomerAddress;