import React, { Component } from 'react';
import Axios from "axios";
import classNames from 'classnames';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { UserService } from '../auth/UserServices';

class CustomerRegister extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      email:'',
    };
  }

  handleInputChange = (event) => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  }
  onSubmit = async (event) => {
    try {
      event.preventDefault();
      const { username, password,email } = this.state;
      let response = await Axios.post('/api/v1/customers', {
        username,
        password,
        email
      });


      if (response.status === 201) {
        UserService.adminLogout();
        localStorage.setItem('customerId', response.data.customerId);
        localStorage.setItem('token', response.data.token);
        this.props.history.push('/');
        window.location.reload();
      }
    } catch (error) {
      console.log(error);
      alert('Failure to register');
    }

  }
  render() {
    const { username, password,email } = this.state;
    return (
      <div className={classNames('d-flex', 'flex-column', 'align-items-center')}>
        <h1>Sign Up</h1>
        <Form onSubmit={e => this.onSubmit(e)} className={classNames('w-50')}>
          <FormGroup>
            <Label for="username" className={classNames('font-weight-bold')}>Username</Label>
            <Input type="text" name="username" id="username" placeholder=""
              value={username}
              onChange={this.handleInputChange}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for="password" className={classNames('font-weight-bold')}>Password</Label>
            <Input type="password" name="password" id="password"
              value={password}
              onChange={this.handleInputChange}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label for="email" className={classNames('font-weight-bold')}>Email</Label>
            <Input type="email" name="email" id="email"
              value={email}
              onChange={this.handleInputChange}
              required
            />
          </FormGroup>
          <Button>Sign Up</Button>
        </Form>
      </div>
    );
  }
}

export default CustomerRegister;
