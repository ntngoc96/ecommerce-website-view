import React, { Component } from 'react';
import Axios from 'axios';
import classNames from 'classnames';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, Button, Row, Col, Spinner
} from 'reactstrap';
import { NavLink } from "react-router-dom";



class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CartItems: [],
      totalPrice: 0,
      totalQuantity: 0
    }
  }

  increaseItem = (productId) => {
    Axios.get(`/api/v1/carts/${productId}`).then((response) => {

      if (response.status === 200) {
        let { cart: CartItems, totalQuantity, totalPrice } = response.data;
        this.setState((state, props) => { return { CartItems, totalQuantity, totalPrice } });

      } else {
        const error = new Error(response.error);
        throw error;
      }
    })
      .catch((error) => {
        console.log(error);
        alert('Increase item error');
      });
  }

  decreaseItem = (productId) => {
    Axios.delete(`/api/v1/carts/${productId}`).then((response) => {
      console.log(response);

      if (response.status === 200) {
        let { cart: CartItems, totalQuantity, totalPrice } = response.data;
        this.setState((state, props) => { return { CartItems, totalQuantity, totalPrice } });

      } else {
        const error = new Error(response.error);
        throw error;
      }
    })
      .catch((error) => {
        console.log(error);
        alert('Decrease item error');
      });
  }
  
  onDeleteAllClicked = async () =>{
    try {
      let response = await Axios.delete(`api/v1/carts/empty/all`);
      if (response.status === 200) {
        this.props.history.push('/')
      }
    } catch (error) {
      alert('Failure to delete all cart items')
    }
  }

  componentDidMount() {
    Axios.get('/api/v1/carts').then(res => this.setState((state, props) => {
      let { cart: CartItems, totalPrice, totalQuantity } = res.data;
      return { CartItems, totalQuantity, totalPrice }
    }))
  }

  render() {
    let { CartItems, totalPrice, totalQuantity } = this.state;

    return (
      <div>
        <Button className="btn-danger m-2" onClick={this.onDeleteAllClicked}>Delete All Cart</Button>
        {CartItems.length === 0 && <Spinner color="primary" />}

        {CartItems.length !== 0 && <Row>
          <Col xs="9">
            {CartItems.map((value, index) =>
              <Row key={value.product.productId}>
                <Col xs="12" >
                  <Card className={classNames('d-flex', 'flex-row', 'p-4')}>
                    <CardImg className={classNames('w-20r','h-20r')} src={value.product.productImages[0].imageUrl} alt="Card image cap" />
                    <CardBody className={classNames('w-50')}>
                      <CardTitle>{value.product.name}</CardTitle>
                      <CardText className={classNames('u-text-line-though')}>Price: {value.product.unitPrice} USD </CardText>
                      <CardText>Discount: {value.product.discount}%</CardText>

                    </CardBody>
                    <CardBody className={classNames('w-50')}>
                      <CardText>{Math.round(((value.product.unitPrice - value.product.unitPrice*value.product.discount/100)*value.quantity)*1000)/1000} USD </CardText>
                      <Button onClick={e => this.decreaseItem(value.product.productId)}>-</Button>
                      <span className={classNames('px-3')}>{value.quantity}</span>
                      <Button onClick={e => this.increaseItem(value.product.productId)}>+</Button>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            )}
          </Col>
          <Col xs="3" className={classNames('position-relative')}>
            <div className={classNames('d-flex','w-25', 'flex-column', 'p-2', 'position-fixed')}>
              <div>Total quantity: {totalQuantity}</div>
              <div>Total price: {totalPrice}</div>
              <NavLink to="/checkout/address"><Button>Start Order</Button></NavLink>
            </div>
          </Col>
        </Row>
        }
      </div>
    );
  }
}

export default Cart;