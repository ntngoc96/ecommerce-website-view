import React, { Component } from 'react';
import Axios from 'axios';
import {
  Table, Spinner, Button, Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';
import { NavLink } from "react-router-dom";

class Order extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Orders: [],
      OrderDetails: [],
      modal: false
    }

  }

  toggle = async () => {
    let { modal } = this.state;
    this.setState((state, props) => { return { modal: !modal } });
  }

  onDetailClicked = async (orderId) => {
    try {
      let { modal } = this.state;
      let response = await Axios.get(`/api/v1/orders/${orderId}/details`);
      if (response.status === 200) {
        this.setState((state, props) => { return { modal: !modal, OrderDetails: response.data.orderDetails } });
      }

    } catch (error) {
      console.log(error);
      alert("Display product detail error")
    }
  }

  onDeleteClicked = async (orderId) => {
    try {
      const customerId = localStorage.getItem('customerId')
      let response = await Axios.delete(`/api/v1/customers/${customerId}/orders/${orderId}`);
      if (response.status === 200) {
        alert("Delete order successfully")
        window.location.reload();
      }

    } catch (error) {
      console.log(error);
      alert("Delete order error")
    }

  }

  onContinueClicked = (orderId) => {
    localStorage.setItem('orderId', orderId);
  }

  discountUnitPrice = (order) => {
    let { orderDetails } = order;
    let amount = 0;
    for (let index = 0; index < orderDetails.length; index++) {
      amount += (orderDetails[index].unitPrice - (orderDetails[index].unitPrice * orderDetails[index].discount) / 100)* orderDetails[index].quantity
      amount = Math.round(amount*1000)/1000;
    }

    return amount;
  }

  async componentDidMount() {
    const customerId = localStorage.getItem('customerId')
    let orders = await Axios.get(`/api/v1/customers/${customerId}/orders?offset=0&limit=50`);

    this.setState((state, props) => { return { Orders: orders.data } });

  }

  

  render() {
    let { Orders, modal, OrderDetails } = this.state;
    let className = "";

    return (
      <div>
        <div>
          <Modal isOpen={modal} toggle={this.toggle} className={className}>
            <ModalHeader >List Order Detail</ModalHeader>
            <ModalBody>
              <Table size="sm">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Order Detail Id</th>
                    <th>Product Id</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Discount</th>
                  </tr>
                </thead>
                <tbody>
                  {OrderDetails.length !== 0 && OrderDetails.map((value, index) =>
                    <tr key={index}>
                      <th scope="row">{index}</th>
                      <td>{value.orderDetailId}</td>
                      <td>{value.productId}</td>
                      <td>{value.quantity}</td>
                      <td>{value.unitPrice}</td>
                      <td>{value.discount}</td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={this.toggle}>Close</Button>
            </ModalFooter>
          </Modal>
        </div>

        <h1>Order History</h1>
        {Orders.length === 0 && <Spinner color="primary" />}
        {Orders.length !== 0 && <Table borderless>
          <thead>
            <tr>
              <th>#</th>
              <th>Order Id</th>
              <th>Date Order</th>
              <th>Payment Id</th>
              <th>Message</th>
              <th>Order Status</th>
              <th>Total</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {Orders.map((value, index) => <tr key={index}>
              <th scope="row">{index + 1}</th>
              <td>{value.orderId}</td>
              <td>{value.dateOrder}</td>
              <td>{value.paymentId}</td>
              <td>{value.message}</td>
              <td>{value.orderStatus === 1 ? 'paid' : 'unpaid'}</td>
              <td>{this.discountUnitPrice(value)}</td>
              <td><div className="d-flex flex-row">
                <NavLink to="/customers/payment" ><Button className="btn-info" disabled={value.orderStatus === 1 ? true : false} onClick={e => this.onContinueClicked(value.orderId)} >Continue</Button></NavLink>
                <Button className="btn-success" onClick={e => this.onDetailClicked(value.orderId)}>Detail</Button>
                <Button className="btn-danger" onClick={e => this.onDeleteClicked(value.orderId)}>Delete</Button>
              </div></td>
            </tr>)}
          </tbody>
        </Table>
        }
      </div>
    );
  }
}

export default Order;