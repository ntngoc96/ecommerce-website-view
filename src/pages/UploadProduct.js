import React from 'react';
import Axios from 'axios';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';


class UploadProduct extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      producer: '',
      unitPrice: '',
      discount: 0,
      categories: [],
      categoryId: '',
      files: []

    }
  }

  onInputChange = (e) => {
    let { value, name } = e.target;
    this.setState((state, props) => { return { [name]: value } });
  }
  onFileChange = (e, i) => {
    this.setState({ files: [...this.state.files, ...e.target.files] })
  }


  onSubmit = async (e) => {
    try {
      e.preventDefault();
      let { name, producer, unitPrice, discount, categoryId,files } = this.state;
      const data = new FormData();


      data.append('name', name);
      data.append('producer', producer);
      data.append('unitPrice', unitPrice);
      data.append('discount', discount);
      data.append('categoryId', categoryId);
      
      for (const iterator of files) {
        data.append('files',iterator)
      }

      let response = await Axios.post(`/api/v1/products`, data);
      if (response.status === 201) {
        alert('Upload successfully');
        this.setState((state, props) => { return { name: '', producer: '', unitPrice: 0, discount: 0, categoryId: '', files:[] } });

      }
    } catch (error) {
      console.log(error);
      alert('Failure to upload product')
    }

  }

  async componentDidMount() {
    try {
      let response = await Axios.get('/api/v1/categories');
      if (response.status === 200) {
        const { data: categories } = response;
        this.setState((state, props) => { return { categories } });
      }
    } catch (error) {
      console.log(error);
      alert('Loading categories fail');
    }
  }

  render() {
    let { name, producer, unitPrice, discount, categories, categoryId } = this.state;
    return (
      <div>
        <h1 className="text-center">Upload Product</h1>
        <Form onSubmit={e => { this.onSubmit(e) }}>
          <FormGroup>
            <Label for="name">Product name</Label>
            <Input type="text" name="name" id="name"
              value={name}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. IphoneX, Samsung Galaxy Note 10" />
          </FormGroup>
          <FormGroup>
            <Label for="producer">Producer</Label>
            <Input type="text" name="producer" id="producer"
              value={producer}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. Apple, Samsung" />
          </FormGroup>
          <FormGroup>
            <Label for="unitPrice">Product unitPrice</Label>
            <Input type="number" name="unitPrice" id="unitPrice"
              value={unitPrice}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. 100, 200, 500" />
          </FormGroup>
          <FormGroup>
            <Label for="discount">Discount (percentage)</Label>
            <Input type="number" name="discount" id="discount"
              value={discount}
              onChange={e => this.onInputChange(e)}
              placeholder="eg. 5, 10, 20" />
          </FormGroup>
          <FormGroup>
            <Label for="categoryId">Select</Label>
            <Input type="select" name="categoryId" id="categoryId"
              value={categoryId}
              onChange={e => this.onInputChange(e)}
            >
              <option value="">Choose categories</option>
              {categories.map((value, index) => <option key={value.categoryId} value={value.categoryId}>{value.name}</option>)}
            </Input>
          </FormGroup>
          <FormGroup>
            <Label for="image">List Product Images </Label>
            <Input type="file" name="image" id="image"
              onChange={e => this.onFileChange(e)} multiple/>
          </FormGroup>
          <Button >Upload</Button>
        </Form>
      </div>
    )
  }

}


export default UploadProduct;