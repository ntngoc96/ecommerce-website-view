
// @flow 
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    (localStorage.getItem('token') !== null)
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/customers/signin', state: { from: props.location } }} />
  )} />
);

export const PrivateAdminRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    (localStorage.getItem('token') && localStorage.getItem('userId'))
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/customers/signin', state: { from: props.location } }} />
  )} />
);