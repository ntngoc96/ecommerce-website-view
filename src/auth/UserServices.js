function logout() {
  try {
    localStorage.removeItem('token');
    localStorage.removeItem('customerId');
    localStorage.removeItem('addressId');
    localStorage.removeItem('orderId');
    window.location.reload();
  } catch (error) {
    console.log(error);
    alert('logout error')
  }
}

function adminLogout() {
  try {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('addressId');
    localStorage.removeItem('orderId');
    window.location.href = "/"
  } catch (error) {
    console.log(error);
    alert('logout error')
  }
}

export const UserService = {
  logout,
  adminLogout
}